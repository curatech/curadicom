//
//  cmove2.swift
//  DicomImageProcessing
//
//  Created by Apple on 10/7/18.
//  Copyright © 2018 Genix. All rights reserved.
//
import Foundation


func setup(){
    
//    let serverAddress = "www.dicomserver.co.uk"
//    let serverPort = "11112"
//    let serverAET = "AWSPIXELMEDPUB"

    let serverAddress = "192.168.6.204"
    let serverPort = "4242"
    let serverAET = "ORTHANC"
    
    let destinationAET = "MYCOMPUTER"
    // If empty then the SCP will accept associations for any called AET
    let ourAET = ""
    let ourPort = "8089"
    
    let instanceUID = "dicomfile"
    let classUID = ImebraUidPatientRootQueryRetrieveInformationModelMOVE_1_2_840_10008_5_1_4_1_2_1_2
    
    do
    {
        // Open a TCP stream used to receive the data
        let tcpListener = try ImebraTCPListener(address: ImebraTCPPassiveAddress(node: "", service: ourPort))
        
        // Launch a tread that will receive the moved data
        DispatchQueue.global(qos: .background).async
            {
                do
                {
                    // Wait for an incoming connection. Will throw if the listener is closed
                    // before a connection arrives
                    let tcpStream = try tcpListener.waitForConnection()
                    
                    // Allocate a reader and a writer that read and write into the TCP stream
                    let readScp = ImebraStreamReader(inputStream: tcpStream)
                    let writeScp = ImebraStreamWriter(inputOutputStream: tcpStream)
                    
                    // Tell that we accept the class UID specified in the command line, and which
                    // transfer syntaxes we can handle for that class
                    let context = ImebraPresentationContext(abstractSyntax: classUID)
                    context?.addTransferSyntax(ImebraUidImplicitVRLittleEndian_1_2_840_10008_1_2)
                    context?.addTransferSyntax(ImebraUidExplicitVRLittleEndian_1_2_840_10008_1_2_1)
                    context?.addTransferSyntax(ImebraUidJPEGBaselineProcess1_1_2_840_10008_1_2_4_50)
                    context?.addTransferSyntax(ImebraUidJPEGExtendedProcess2_4_1_2_840_10008_1_2_4_51)
                    context?.addTransferSyntax(ImebraUidJPEGLosslessNonHierarchicalProcess14_1_2_840_10008_1_2_4_57)
                    context?.addTransferSyntax(ImebraUidJPEGLosslessNonHierarchicalFirstOrderPredictionProcess14SelectionValue1_1_2_840_10008_1_2_4_70)
                    let presentationContexts = ImebraPresentationContexts()
                    presentationContexts?.add(context)
                    
                    // Negotiate the association with the SCU (we act as SCP here)
                    let scp = try ImebraAssociationSCP(thisAET: ourAET,
                                                       maxInvokedOperations: 1,
                                                       maxPerformedOperations: 1,
                                                       presentationContexts: presentationContexts,
                                                       reader: readScp,
                                                       writer: writeScp,
                                                       dimseTimeoutSeconds: 30,
                                                       artimTimeoutSeconds:30)
                    
                    // Use a DIMSE service to send and receive commands/response
                    let scpDimseService = ImebraDimseService(association: scp)
                    
                    // We wait for just one command
                    let command = try scpDimseService!.getCommand()
                    
                    if command.commandType == ImebraDimseCommandType_t.cStore
                    {
                        let dataSet = try command.getPayloadDataSet()
                        try ImebraCodecFactory.save(toFile: instanceUID, dataSet: dataSet, codecType: ImebraCodecType_t.dicom)
                    }
                    
                    try scp.release()
                }
                catch let error as ImebraStreamError
                {
                    print("ImebraStreamError")
                    print("Error: \(error.domain)")
                    print(error.userInfo[NSUnderlyingErrorKey])
                }
                catch let error as NSError
                {
                    print("NSError")
                    print("Error: \(error.domain)")
                    print(error.userInfo[NSUnderlyingErrorKey])
                }
        }
        
        
        //---------------------------------------
        //
        // Create the SCU
        //
        //---------------------------------------
        
        // Open a TCP stream connected to the other SCP
        let tcpStream = try ImebraTCPStream(address: ImebraTCPActiveAddress(node: serverAddress, service: serverPort))
        
        // Allocate a reader and a writer that read and write into the TCP stream
        let readScu = ImebraStreamReader(inputStream: tcpStream)
        let writeScu = ImebraStreamWriter(inputOutputStream: tcpStream)
        
        // Tell that we want to use C-MOVE
        let context = ImebraPresentationContext(abstractSyntax: classUID)
        context?.addTransferSyntax(ImebraUidImplicitVRLittleEndian_1_2_840_10008_1_2)
        context?.addTransferSyntax(ImebraUidExplicitVRLittleEndian_1_2_840_10008_1_2_1)
        let presentationContexts = ImebraPresentationContexts()
        presentationContexts?.add(context)
        
        // Negotiate the association with the SCP
        let scu = try ImebraAssociationSCU(thisAET: ourAET,
                                           otherAET: serverAET,
                                           maxInvokedOperations: 1,
                                           maxPerformedOperations: 1,
                                           presentationContexts: presentationContexts,
                                           reader: readScu,
                                           writer: writeScu,
                                           dimseTimeoutSeconds: 0)
        
        // Use a DIMSE service to send and receive commands/response
        let scuDimseService = ImebraDimseService(association: scu)
        
        // Create a datase where we set the matching tags used to find the instances to move
        let identifierDataset = ImebraDataSet(transferSyntax: ImebraUidImplicitVRLittleEndian_1_2_840_10008_1_2)
        try identifierDataset!.setString(ImebraTagId(id: ImebraTagId_t.tagSOPInstanceUID_0008_0018), newValue: instanceUID)
        
        // Prepare a C-MOVE command and send it to the SCP
        let moveCommand = ImebraCMoveCommand(abstractSyntax: classUID,
                                             messageID: scuDimseService!.getNextCommandID(),
                                             priority: ImebraDimseCommandPriority_t.priorityMedium,
                                             affectedSopClassUid: ImebraUidPatientRootQueryRetrieveInformationModelMOVE_1_2_840_10008_5_1_4_1_2_1_2,
                                             destinationAET:destinationAET,
                                             identifier: identifierDataset)
        
        try scuDimseService!.sendCommandOrResponse(moveCommand)
        
        // Wait for a response to the C-MOVE command
        let moveResponse = try scuDimseService!.getCMoveResponse(moveCommand)
        if moveResponse.status == ImebraDimseStatus_t.success
        {
            print("OK")
        }
        else
        {
            print("Error")
        }
        
        // Release gracefully
        try scu.release()
        
    }
    catch let error as ImebraStreamError
    {
        print("ImebraStreamError")
        print("Error: \(error.domain)")
        print(error.userInfo[NSUnderlyingErrorKey])
    }
    catch let error as NSError
    {
        print("NSError")
        print("Error: \(error.domain)")
        print(error.userInfo[NSUnderlyingErrorKey])
    }

}



//        do{
//            let tcpStream = try ImebraTCPStream(address: ImebraTCPActiveAddress(node: "www.dicomserver.co.uk", service: "11112"))
//            let readScu = ImebraStreamReader(inputStream: tcpStream)
//            let writeScu = ImebraStreamWriter(inputOutputStream: tcpStream)
//            let context = ImebraPresentationContext(abstractSyntax: ImebraUidEnhancedMRImageStorage_1_2_840_10008_5_1_4_1_1_4_1)
//            context?.addTransferSyntax("1.2.840.10008.1.2.1")
//            let presentationContexts = ImebraPresentationContexts()
//            presentationContexts?.add(context)
//            let scu = try ImebraAssociationSCU(thisAET: "SCU", otherAET: "AWSPIXELMEDPUB", maxInvokedOperations: 1, maxPerformedOperations: 1, presentationContexts: presentationContexts, reader: readScu, writer: writeScu, dimseTimeoutSeconds: 0)
//
//            let res = try scu.getResponse(1)
//
//        } catch let error as ImebraStreamError {
//            print("ImebraStreamError")
//            print("Error: \(error.domain)")
//            print(error.userInfo[NSUnderlyingErrorKey])
//        } catch let error as NSError {
//            print("NSError")
//            print("Error: \(error.domain)")
//            print(error.userInfo[NSUnderlyingErrorKey])
//        }
